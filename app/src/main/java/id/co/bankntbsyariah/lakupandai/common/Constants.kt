package id.co.bankntbsyariah.lakupandai.common

class Constants {

    companion object {
        const val BASE_API= "https://pipboy5k.serveirc.com/arrest/"

        const val DEFAULT_VERSION = 1
        const val DEFAULT_ROOT_ID = "L0000"

        const val SP_APPS = "ntbs storage"
        const val SP_VERSION = "stored version"
        const val SP_ROOT_ID = "root menu id"

        const val MAX_CHECK_UPDATE_PROCESS_STAGE = 100

        const val KEY_MENU_ID = "menu id holder"
        const val KEY_FORM_ID = "form id holder"

        const val SCREEN_TYPE_MENU = 0
        const val SCREEN_TYPE_FORM = 1
        const val SCREEN_TYPE_POPUP_SUKSES = 2
        const val SCREEN_TYPE_POPUP_GAGAL = 3
        const val SCREEN_TYPE_POPUP_LOGOUT = 4

        const val ELE_TYPE_TEXT = 0

    }
}
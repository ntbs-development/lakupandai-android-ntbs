package id.co.bankntbsyariah.lakupandai.common

data class Component(
    var visible: Boolean,
    var type: Int,
    var id: String,
    var label: String,
    var action: String,
    var icon: String,
    var desc: String,
    var seq: Int
)
